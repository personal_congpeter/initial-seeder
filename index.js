/*!
 *  ExpressLoad Stub.
 *
 *  @author Jarrad Seers <jarrad@jarradseers.com>
 *  @created 07/08/2012
 */
var express = require('express'), load = require('express-load'), sequelize = require('sequelize');
var app = express();

load('config').into(app);
for (var environment in app.config) {
  if (environment == app.get('env')) {
    for (var key in app.config[environment]) {
      app.set(key, app.config[environment][key]);
    }
  }
}
/**
 *  Autoload models, controllers and routes into application instance.
 */

load('models')
  .then('controllers')
  .then('routes')
  .into(app);

  /**
 *  Listen on the configured port.
 */

app.listen(app.get('port'));

/**
 *  Display some stuff from the auto-loaded config.
 */

console.log('%s running in %s mode on port %s' 
  , app.get('title')
  , app.get('env')
  , app.get('port')
);
module.exports = require('./lib/express-load.js');